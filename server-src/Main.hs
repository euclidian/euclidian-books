{-# LANGUAGE CPP           #-}
{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeFamilies  #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}
module Main where

-- Basedon the tutorial from http://rundis.github.io


import Data.Aeson
import GHC.Generics
import Data.List
import Network.Wai
import Network.Wai.Handler.Warp
import Servant


data Book = Book
  { bookId :: Int
  , bookName :: String
  , bookAuthor :: String
  , loanedTo :: Maybe Person
  , bookFinished :: Bool
  } deriving (Eq, Show, Generic)

data Person = Person 
  { personId :: Int
  , personName :: String
  } deriving (Eq, Show, Generic)

instance ToJSON Book
instance ToJSON Person

type BookAPI =
       Get '[JSON] [Book]
  :<|> Capture "bookId" Int :> Get '[JSON] Book


bookServer :: Server BookAPI
bookServer = getBooks :<|> getBook

  where getBooks :: Handler [Book]
        getBooks = return books

        getBook :: Int -> Handler Book
        getBook id = case (bookById id) of
            Nothing -> throwError (err404 {errBody = "No book with given id exists"})
            Just b -> return b



bookById :: Int -> Maybe Book
bookById idParam = find ((== idParam) . bookId) books


books :: [Book]
books =
  [ Book 1 "Heirs to forgotten kingdoms" "Gerard Russel" (Just mum) True
  , Book 2 "Complete works of Plato" "Plato" Nothing False
  ]

mum = Person 1 "Mum"

type API = "api" :> BookAPI


api :: Proxy API
api = Proxy


app :: Application
app = serve api bookServer


main :: IO ()
main = run 8081 app
